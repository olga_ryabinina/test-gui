
#ifndef VIB_NAME_H
#define VIB_NAME_H

#include <qdialog.h>
#include <qtextcodec.h>
#include <qradiobutton.h>
#include <qbuttongroup.h>
#include <qmessagebox.h>
#include <qlayout.h>
#include <qpushbutton.h>

class Vib_name : public QDialog  {

 Q_OBJECT
public: 
	Vib_name(QWidget *parent = 0, const char *name = 0);
	~Vib_name();

public slots:
 void slot_Ok();
 	
private:
 QTextCodec *codec;
 QRadioButton *rb11;
 QButtonGroup *grp;

};

#endif
