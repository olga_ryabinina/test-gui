/****************************************************************************
** Vib_name meta object code from reading C++ file 'vib_name.h'
**
** Created: Tue May 12 10:09:23 2020
**      by: The Qt MOC ($Id: qt/moc_yacc.cpp   3.3.8   edited Feb 2 14:59 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "vib_name.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.8b. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *Vib_name::className() const
{
    return "Vib_name";
}

QMetaObject *Vib_name::metaObj = 0;
static QMetaObjectCleanUp cleanUp_Vib_name( "Vib_name", &Vib_name::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString Vib_name::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "Vib_name", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString Vib_name::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "Vib_name", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* Vib_name::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QDialog::staticMetaObject();
    static const QUMethod slot_0 = {"slot_Ok", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "slot_Ok()", &slot_0, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"Vib_name", parentObject,
	slot_tbl, 1,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_Vib_name.setMetaObject( metaObj );
    return metaObj;
}

void* Vib_name::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "Vib_name" ) )
	return this;
    return QDialog::qt_cast( clname );
}

bool Vib_name::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: slot_Ok(); break;
    default:
	return QDialog::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool Vib_name::qt_emit( int _id, QUObject* _o )
{
    return QDialog::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool Vib_name::qt_property( int id, int f, QVariant* v)
{
    return QDialog::qt_property( id, f, v);
}

bool Vib_name::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
