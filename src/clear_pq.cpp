
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <qapplication.h>
#include <iostream>
#include <qstring.h>
#include <qtextcodec.h>
#include <qmessagebox.h>
#include "opq.h"
#include "opq_struct.h"
#include <sys/ipc.h>
#include "rio.h"
#include "moment.h"
#include "uvs_struct.h"
#include "vib_name.h"

// ������ ��� �������� ������ � ����������� � GitLab

int main(int argc, char *argv[])
{
int snp, par;

  QApplication app(argc, argv);
   QTextCodec * codec = QTextCodec::codecForName("KOI8-R");
     
   app.setDefaultCodec(QTextCodec::codecForName("KOI8-R"));

//  QFont ffont("Helvetica",12,QFont::Normal, FALSE);
QFont ffont("Helvetica",10,QFont::Normal, FALSE);
  ffont.setFamily( "Helvetica");
  ffont.setPixelSize(10);
  ffont.setWeight(QFont::Normal); 
  app.setFont(ffont);

   Vib_name vto;
   vto.setGeometry(300,80,200,150);
   vto.setCaption(codec->toUnicode("��������"));
   par = vto.exec();

 if( par == 1)
  {
typedef struct PQ
{
BYTE work:1;             // ������� ������
BYTE to:1;            // ��� ���������
                      // 0-�������,1-�������
BYTE group:3;           // ����� ���������� ������
BYTE rez:3;
} PQ;

PQ val;

   if(( snp = N_Process(COZ)) <= 0)     //�������� ��������� (������ �������) ����� ��������
    {
     QMessageBox::about(0,codec->toUnicode("��������"),codec->toUnicode("�� ������� ����� �������� ���"));
//     cout<<"�� ������� ����� �������� ���"<<endl;
     return (0);
    }

   val.work = 0;
   val.to=0;
   val.group = 0;
   val.rez = 0;
   write_pq( KCOZ_NB, KCOZ_LB, &val, 1);
  }
 else
  {
   if( par == 2)
    {
     if(( snp = N_Process(SNK)) <= 0)     //�������� ��������� (������ �������) ����� ��������
      {
       QMessageBox::about(0,codec->toUnicode("��������"),codec->toUnicode("�� ������� ����� �������� ���"));
//       cout<<"�� ������� ����� �������� SNK"<<endl;
       return (0);
      }
char val;
      val = '0';
      write_pq( SNK_NB, SNK_LB, &val, 1);
    }
   else
    QMessageBox::about(0,codec->toUnicode("��������"),codec->toUnicode("��� �������� �� �������"));

  }
 exit(0);

}
