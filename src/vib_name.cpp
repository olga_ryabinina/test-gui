
#include "vib_name.h"

Vib_name::Vib_name(QWidget *parent, const char *name)
         : QDialog(parent, name)
{
 codec = QTextCodec::codecForName("KOI8-R");

  QVBoxLayout *vbox = new QVBoxLayout( this );
  vbox->setMargin( 7 );
  grp = new QButtonGroup( 1,QGroupBox::Horizontal,codec->toUnicode("����� ����� ��������"), this);
  vbox->addWidget( grp );
  grp->setExclusive( TRUE );

    // insert 2 radiobuttons
  rb11 = new QRadioButton(codec->toUnicode("���"), grp );
  rb11->setChecked( TRUE );
  (void)new QRadioButton( codec->toUnicode("���"), grp );

  vbox->insertSpacing (-1, 7);

  QHBoxLayout *hbox = new QHBoxLayout(vbox);
  QPushButton *OkButton = new QPushButton(codec->toUnicode("��"), this);
  hbox->addWidget(OkButton);
  hbox->insertSpacing (0, 10);

  QPushButton *CancelButton = new QPushButton(codec->toUnicode("������"), this);
  hbox->addWidget(CancelButton);
  CancelButton->setDefault(TRUE);	
  hbox->insertSpacing (-1, 10);
  	
  connect(OkButton, SIGNAL(clicked()), SLOT(slot_Ok()));
  connect(CancelButton, SIGNAL(clicked()), SLOT(reject()));
}

void Vib_name::slot_Ok()
{
int nom;
 nom = grp->id(grp->selected()) + 1;
 done(nom);
}

Vib_name::~Vib_name(){ }


